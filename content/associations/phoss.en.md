---
title: "PhOSS"
---

PhOSS, the Physics Open Source Science, is the group behind the project and most of its development.

Its objective consist in creating and sharing tools, scripts, programs and documentation related to science and technology with a primary focus on Physics and Engineering.