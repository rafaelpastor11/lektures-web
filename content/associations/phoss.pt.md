---
title: "PhOSS"
---

PhOSS, Physics Open Source Science, é o grupo que criou e desenvolve a maioria do projeto.

O seu objetivo consiste em criar e partilhar ferramentas, scripts, programas e documentação relacionada com ciência e tecnologia, com especial foco na Física e Engenharia.