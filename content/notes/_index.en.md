---
title: "Notes"
description: "This is the page that contains all notes"
pre: <b><i class="fas fa-book-open"></i> </b>
weight: 2
---

This is the directory that contains all notes