---
title: "Contribuições"
description: "Página das contribuições"
pre: <b><i class="fas fa-people-carry"></i> </b>
weight: 3
---

Esta página refere as contribuições para o projeto.
Aqui pode encontrar-se informação sobre como contribuir e sobre quem já contribui.