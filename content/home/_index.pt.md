---
title: "Home"
description: "Página inicial"
pre: <b><i class="fas fa-home"></i> </b>
weight: 1
---

Bem-vindo à página inicial do projeto lektures!
Este projeto é desenvolvido pelo grupo PhOSS.