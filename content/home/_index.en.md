---
title: "Home"
description: "Main page"
pre: <b><i class="fas fa-home"></i> </b>
weight: 1
---

Welcome to the lektures project home page!
This project is developed by the PhOSS group.